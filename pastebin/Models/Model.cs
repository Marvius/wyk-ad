﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace pastebin.Models
{
    public class Model
    {
        public int Id { get; set; }
        public string LinkKey { get; set; }
        public string Content { get; set; }
    }
}