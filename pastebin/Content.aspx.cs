﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using pastebin.Context;
using pastebin.Models;

namespace pastebin
{
    public partial class Content : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string key = Page.RouteData.Values["key"].ToString();
            using(var db = new PastDbContext())
            {
                var content = db.MainModel.SingleOrDefault(m => m.LinkKey.Equals(key)).Content;
                text.Text = content;
            }
        }
    }
}