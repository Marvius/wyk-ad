﻿using System.Data.Entity;
using pastebin.Models;

namespace pastebin.Context
{
    public class PastDbContext : DbContext
    {
        public PastDbContext()
            : base("pastebin")
        {
        }
        public DbSet<Model> MainModel { get; set; }
    }
}