﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using pastebin.Context;
using pastebin.Models;

namespace pastebin
{
    public partial class _Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void submitButton_Click(object sender, EventArgs e)
        {
            var content = TextArea.Text;
            var key = Path.GetRandomFileName().Replace(".", "");

            if (!String.IsNullOrEmpty(content))
            {
                using (var db = new PastDbContext())
                {
                    Model m = new Model
                    {
                        LinkKey = key,
                        Content = content
                    };

                    db.MainModel.Add(m);
                    db.SaveChanges();

                    Response.Redirect(String.Format("/{0}", key));
                }
            }
        }
    }
}